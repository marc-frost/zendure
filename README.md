# Zendure into Home Assistant


## General information

Question: Where are server hosted
> AWS Hong Kong  https://www.shodan.io/host/18.167.233.54

Question: MQTT 
> maybe RabbitMQ

QuestionQ: How does the time control work
> Event controlled via mqtt (only works if the hub receives the event at the time of switching)

Question: you can send an event yourself
> currently not, own sended events are not displayed in the queue

Question: Where are all data located (images, manuels, firmwares)
> https://zendure-app.s3.amazonaws.com/

## How to get the api key

Instruction  -> https://github.com/Zendure/developer-device-data-report


### Where is the snNumber?

![](images/2023-07-27%2017_37_11-Zendure.png){height=40%}
![](images/2023-07-27%2017_39_23-Zendure.png){height=40%}
![](images/2023-07-27%2017_40_06-Zendure.png){height=40%}


### Where is the account?

Your app email from registration process.

#### Who do get the API key

Java, C# Sample -> https://github.com/Zendure/developer-device-data-report/tree/master/src/main/java/org/zendure/examples/api


cli (linux)
```
wget --post-data '{"snNumber": "","account": "EMAIL"}' --header='Content-Type:application/json' -S -O - https://app.zendure.tech/v2/developer/api/apply

```


![](images/2023-07-27%2017_54_59-zendure.png){height=40%}


remember appKey + secret


## Setup MQTT Bridge in Home Assistant (emqx version)

Open emqx in Home Assist, go to Integration -> Data Bridge, click on create MQTT.

```
Name: Zendure (you can change if your want)
MQTT Broker: mqtt.zen-iot.com:1883
Username: appKey from API
Password: secret from API

Ingress: Yes

Remote MQTT Broker
Topic: appKey/#

Local MQTT Broker
Topic: $shared/zendure (you can change if your want) 
Payload: ${payload}
```


all other field can be by default

![](images/2023-07-27%2018_14_19-zendure.png){height=40%}


## Map Json into Home Assist Values (Sensors)

Open home assist configuration

this mapped the json from Zendure into home assist sensor value

```
sensor:
  - name: "Zendure - Solar Input Power"
    unique_id: solarInputPower
    state_topic: "$shared/zendure"
    unit_of_measurement: "W"
    device_class: "power"
    value_template: "{{ value_json.solarInputPower }}"
  - name: "Zendure - Home Output Power"
    unique_id: outputHomePower
    state_topic: "$shared/zendure"
    unit_of_measurement: "W"
    device_class: "power"
    value_template: "{{ value_json.outputHomePower }}"
  - name: "Zendure - Batterie Output Power"
    unique_id: outputPackPower
    state_topic: "$shared/zendure"
    unit_of_measurement: "W"
    device_class: "power"
    value_template: "{{ value_json.outputPackPower }}"
  - name: "Zendure - Batterie Input Power"
    unique_id: packInputPower
    state_topic: "$shared/zendure"
    unit_of_measurement: "W"
    device_class: "power"
    value_template: "{{ value_json.packInputPower }}"
  - name: "Zendure - Batterie Level"
    unique_id: electricLevel
    unit_of_measurement: "%"
    state_topic: "$shared/zendure"
    device_class: "battery"
    value_template: "{{ value_json.electricLevel }}"

```

save + reload config


## Add values into energy dashboard



![](images/2023-07-27%2018_20_54-zendure.png){height=40%}


![](images/2023-07-27%2018_23_18-zendure.png){height=40%}